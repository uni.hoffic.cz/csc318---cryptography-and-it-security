package cz.hoffic.cryptolabs;

public class Main {

  public static void main(String[] args) throws Exception {
//    task1();
    task2();
  }

  private static void task1() throws Exception {
    var aes = new AES();

    var key1 = aes.generateKeyFromPassword("PAWNED");
    var key2 = aes.generateRandomKey();

    var message = "DEVCON4EVA";

    System.out.println(message);
    System.out.println(aes.encrypt(message, key1));
    System.out.println(aes.encrypt(message, key2));
  }

  private static void task2() throws Exception {
    var des = new DES();

    var key1 = des.generateKeyFromPassword("PAWNED");
    var key2 = des.generateRandomKey();

    var message = "DEVCON4EVA";

    var crypt1 = des.encrypt(message, key1);
    var crypt2 = des.encrypt(message, key2);

    System.out.println(message);

    System.out.println(crypt1);
    System.out.println(crypt2);

    System.out.println(des.decrypt(crypt1, key1));
    System.out.println(des.decrypt(crypt2, key2));
  }
}
