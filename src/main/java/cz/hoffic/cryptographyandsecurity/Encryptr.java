package cz.hoffic.cryptographyandsecurity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class Encryptr {

  private static final int ROLL_OVER_THRESHOLD = 26;

  public Encryptr() {
    alphabet = new HashMap<>();

    alphabet.put('_', "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    alphabet.put('A', "ZNYEJDGQFRCWXTVOHIMLPUASBK");
    alphabet.put('B', "JUAXSTGBDCEOVIHRYWLMFNQKZP");
    alphabet.put('C', "YXISPRQGWZTHFVNMLJEOUBKDAC");
    alphabet.put('D', "NYCSTWXZDVOBKHUJAQGRPMLFIE");
    alphabet.put('E', "XTEANRLUJVOIGPMQDKFBSYCWHZ");
    alphabet.put('F', "XQJWONLVKBIFZSTYAHEUMPCDRG");
    alphabet.put('G', "BGPUVNJWCRDAEQHIFSXZTOLMKY");
    alphabet.put('H', "XKVRDIOGYUEJSPAMZCLFWQNHTB");
    alphabet.put('I', "MIZAPXWLKGYCTHROEUBJFSVNDQ");
    alphabet.put('J', "ANYUTSKQJOCLXVHGWERIFPZBMD");
    alphabet.put('K', "SVCFYTLKAOGDUXREPBHMQZWNJI");
    alphabet.put('L', "RKJWVUPIQOBXLNCGAYMDESTFHZ");
    alphabet.put('M', "UYRMAJSFNZEGPKVDTWQICOLXBH");
    alphabet.put('N', "SPTFXIEOKZBWDCMHULNYJGVRAQ");
    alphabet.put('O', "WFTGVYBSLPDZOKAUQCNEJRIMXH");
    alphabet.put('P', "BSZXHCIDQRVKJNAELWFMTPUGOY");
    alphabet.put('Q', "KHPJDUNQGYWABCIMXRLFTEVZSO");
    alphabet.put('R', "LSRIKDXGTWFECAZPJOMUVHNBQY");
    alphabet.put('S', "CIRHLWPSOGVKBJXTMDFEQYNUZA");
    alphabet.put('T', "LZWDGCOFNYEMBTSIPQXHARUJVK");
    alphabet.put('U', "RCJBQGVMLKYSDNEXZUTPWOFIHA");
    alphabet.put('V', "FZHNRDVLGAJCIMSUWTBXOEPKYQ");
    alphabet.put('W', "KOTWYHPRNQZUSCFELIMJBDVAXG");
    alphabet.put('X', "KJHZYGUINVDRWPSOEXMLTABFCQ");
    alphabet.put('Y', "QVFRYPKGAJUWBOMXLDHINECTZS");
    alphabet.put('Z', "GXJINDQFOWSUYZHTPBVLREKMCA");
  }

  public String shiftEncrypt(String input, int key) {
    input = input.toUpperCase();

    char[] output = new char[input.length()];

    int min = charToInt('A');

    char[] letters = input.toCharArray();

    for (int i = 0; i < input.length(); i++) {
      if (letters[i] == ' ') {
        output[i] = ' ';
      } else {
        int val = charToInt(letters[i]);

        val -= min;

        val += key;

        if (val >= ROLL_OVER_THRESHOLD) {
          val -= ROLL_OVER_THRESHOLD;
        } else if (val < 0) {
          val += ROLL_OVER_THRESHOLD;
        }

        val += min;

        output[i] = intToChar(val);
      }
    }

    return new String(output);
  }

  public String shiftDecrypt(String input, int key) {
    return shiftEncrypt(input, -key);
  }

  public String railEncrypt(String input, int key) {
    input = input.toUpperCase();

    char[] letters = input.toCharArray();

    StringBuilder[] rails = new StringBuilder[key];
    for (int i = 0; i < key; i++) {
      rails[i] = new StringBuilder();
    }

    for (int i = 0; i < input.length(); i++) {
      rails[i % key].append(letters[i]);
    }

    StringBuilder output = new StringBuilder();

    for (int i = 0; i < key; i++) {
      output.append(rails[i].toString());
    }

    return output.toString();
  }

  public String railDecrypt(String input, int key) {
    input = input.toUpperCase();

    String[] rails = new String[key];
    int width = (input.length() + input.length() % key) / key;

    for (int i = 0; i < key - 1; i++) {
      rails[i] = input.substring(i * width, (i + 1) * width);
    }
    rails[key - 1] = input.substring((key - 1) * width);

    StringBuilder output = new StringBuilder();

    for (int i = 0; i < width; i++) {
      for (int j = 0; j < key; j++) {
        try {
          output.append(rails[j].charAt(i));
        } catch (IndexOutOfBoundsException e) {
          // That's expected cause I'm too lazy to write a special case for it.
        }
      }
    }

    return output.toString();
  }

  public String columnEncrypt(String input, int key) {
    input = input.toUpperCase();
    int height = (input.length() + input.length() % key) / key;

    char[][] table = new char[key][height];

    for (int i = 0; i < input.length(); i++) {
      table[i % key][i / key] = input.charAt(i);
    }

    var output = new StringBuilder();

    for (int j = 0; j < key; j++) {
      for (int i = 0; i < height; i++) {
        char letter = table[j][i];

        int value = charToInt(letter);

        if (value > 0) {
          output.append(intToChar(value));
        }
      }
    }

    return output.toString();
  }

  public String columnDecrypt(String input, int key) {
    int decryptKey = (input.length() + input.length() % key) / key;

    return columnEncrypt(input, decryptKey);
  }

  private HashMap<Character, String> alphabet;

  public String polyaplhEncrypt(String input, String key) {
    key = key.toUpperCase();
    input = input.toUpperCase();

    HashMap<Character, Character>[] table = new HashMap[key.length()];
    for (int i = 0; i < key.length(); i++) {
      table[i] = new HashMap<>();

      for (int j = 0; j < 26; j++) {
        table[i].put(
            alphabet.get('_').charAt(j),
            alphabet.get(key.charAt(i)).charAt(j)
        );
      }
    }

    StringBuilder output = new StringBuilder();

    for (int i = 0; i < input.length(); i++) {
      output.append(table[i % key.length()].get(input.charAt(i)));
    }

    return output.toString();
  }

  private int charToInt(char input) {
    return (int) input;
  }

  private char intToChar(int input) {
    return (char) input;
  }
}
