package cz.hoffic.cryptographyandsecurity;

public class Main {

  public static void main(String[] args) throws Exception {
    var enc = new Encryptr();

    System.out.println(enc.polyaplhEncrypt("WelcomeToCryptographyLab", "HOF"));
  }
}
